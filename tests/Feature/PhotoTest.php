<?php

namespace Tests\Feature;

use App\Photo;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PhotoTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub
    }

    /**
     * A basic feature test example.
     * @group posts
     * @return void
     */
    public function test_success_get_Photo()
    {
        $user = factory(User::class)->create();
        $photos = factory(Photo::class,10)->create([
            'user_id' => $user->id
        ]);

        $this->actingAs($user);

        $response = $this->get('/');

        $response->assertStatus(200);

        $response->assertSeeText("Photo");
        $response->assertSeeText(config('app.name'));
        $response->assertViewHas('photos');

        foreach ($photos as $photo)
        {
            $response->assertSeeText($photo->title);
        }
    }

    /**
     * A basic feature test example.
     * @group posts
     * @return void
     */
    public function test_success_get_photo_create_form()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $response = $this->get(route('user.store'));
        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     * @group posts
     * @return void
     */
    public function test_failed_get_photo_create_form()
    {
        $response = $this->get(route('user.store'));
        $response->assertStatus(302);
        $response->assertRedirect('login');
    }

}
