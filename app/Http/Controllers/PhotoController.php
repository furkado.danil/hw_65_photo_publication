<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Photo;
use App\User;
use Illuminate\Http\Request;

class PhotoController extends Controller
{

    /**
     * PhotoController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $photos = Photo::all();
        return view('photo.index', compact('photos'));
    }



}
