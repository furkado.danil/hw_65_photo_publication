<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Photo;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Photo $photo
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request, Photo $photo)
    {
        $request->validate([
            'body' => 'required|min:5'
        ]);

        $comment = new Comment();
        $comment->body = $request->input('body');
        $comment->photo_id = $photo->id;
        $comment->user_id = $request->user()->id;
        $comment->save();


        return response()->json([
            'comment' => view('components.comment', compact('comment'))->render()
        ], 202);
    }


    /**
     * @param Request $request
     * @param int $photo_id
     * @param int $comment_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function update(Request $request, int $photo_id, int $comment_id)
    {
        $request->validate([
            'body' => 'required|min:5'
        ]);

        $comment = Comment::findOrFail($comment_id);
        $comment->update($request->all());

        return response()->json([
            'comment' => view('components.comment', compact('comment'))->render()
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $photo_id, $id)
    {
        $comment = Comment::find($id);
        $comment->delete();

        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ], 204);
    }
}
