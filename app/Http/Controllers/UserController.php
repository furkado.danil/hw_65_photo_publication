<?php

namespace App\Http\Controllers;

use App\Http\Requests\PhotoRequest;
use App\Photo;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $users = User::all();
        $users->user_id = $request->user()->id;
        return view('profile.index', compact('users'));
    }



    /**
     * @param PhotoRequest $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PhotoRequest $request)
    {
        $photo = new Photo();
        $file = $request->file('picture');
        if (!is_null($file)) {
            $path = $file->store('images', 'public');
            $photo['picture'] = $path;
        }
        $photo->user_id = $request->user()->id;
        $photo->save();
        return redirect()->route('user.index')->with('Фото загруженно');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($id)
    {
        $photo = Photo::findOrFail($id);
        $photo->delete();

        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ], 204);
    }
}
