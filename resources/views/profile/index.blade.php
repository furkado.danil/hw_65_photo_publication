@extends('layouts.app')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger mt-3">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="mt-4">
            <h3>{{ Auth::user()->name }}</h3>
        </div>
        <div class="ml-5 mt-3">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Загрузить фото
            </button>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form enctype="multipart/form-data" action="{{route('user.store')}}" method="post">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="exampleFormControlFile1">Выбрать фото</label>
                                    <input type="file"  name="picture" class="form-control-file" id="exampleFormControlFile1">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Загрузить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->

    <div class="row mt-5">
        @foreach(Auth::user()->photos as $photo)
            <!-- Modal -->
                <div class="modal fade" id="exampleModal-{{$photo->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <img src="{{asset( '/storage/'. $photo->picture)}}" width="466" alt="{{$photo->picture}}">
                            </div>
                            <div class="modal-footer">
                                <div class="scrollspy-example">
                                    @foreach($photo->comments as $comment)
                                        <x-comment :comment="$comment"></x-comment>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Modal -->

            <div id="photo-delete-{{$photo->id}}">
                <div class="delete">
                    <span data-photo-delete="{{$photo->id}}" class="delete-photo" title="Удалить" aria-hidden="true">&times;</span>
                </div>
                <button type="button" class="btn btn-light" data-toggle="modal"  data-target="#exampleModal-{{$photo->id}}">
                    <img src="{{asset( '/storage/'. $photo->picture)}}" class="rounded mx-auto d-block" width="200" height="200" alt="">
                </button>
                <div class="counter">
                    <span class="badge badge-info" style="cursor: pointer" title="Количество коментариев">{{$photo->comments->count()}}</span>
                </div>
            </div>

        @endforeach
    </div>

@endsection