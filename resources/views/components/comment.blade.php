<div class="comment-block-{{$comment->id}}" id="delete-comment-{{$comment->id}}">
    @can('update', $comment)
        <span data-delete-id="{{$comment->id}}" data-post-delete-id="{{$comment->photo->id}}" class="delete" aria-hidden="true">&times;</span>
    @endcan
    <h5 class="h5 g-color-gray-dark-v1 mb-0">Автор: {{$comment->user->name}}</h5>
    <p>{{$comment->body}}</p>
    @can('update', $comment)
        <button data-comment-id="{{$comment->id}}" id="comment-{{$comment->id}}" class="btn btn-sm btn-primary edit-comment">Изменить</button>
    @endcan
    <form id="edit-comment-form-{{$comment->id}}">
        <div id="comment-e-f{{$comment->id}}" class="edit-form">
            @csrf
            <div class="form-group">
                <label for="body-edit-{{$comment->id}}">Comment</label>
                <textarea name="body" class="form-control" id="body-edit-{{$comment->id}}"  rows="3" required>{{$comment->body}}</textarea>
            </div>
            <button data-photo-id="{{$comment->photo->id}}" data-comment-id="{{$comment->id}}" type="submit" class="btn btn-outline-primary btn-sm btn-block update-comment">Изменить коментарий</button>
        </div>
    </form>
</div>
