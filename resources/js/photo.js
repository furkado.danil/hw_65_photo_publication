$(document).ready(function () {

    $('.delete-photo').on('click', function (event) {
        const photoId = $(this).attr('data-photo-delete');
        const token = $('input[type=hidden]').val();
        const photo = $(`#photo-delete-${photoId}`);
        $.ajax({
            url: `user/${photoId}`,
            method: 'delete',
            data: {_token: token}
        })
            .done(function(response) {
                console.log(response);
                $(photo).remove();
            })
            .fail(function(response) {
                console.log(response);
            });
    })
});