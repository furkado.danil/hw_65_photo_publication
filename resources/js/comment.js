$(document).ready(function () {

    $('.click-comment-btn').click(function (e) {
        e.preventDefault();
        const pathId = $(this).attr('data-photo-id');
        const data = $(`#create-comment-${pathId}`).serialize();
        const photoId = $(`#photo_id-${pathId}`).val();
        $.ajax({
            url: `/photo/${photoId}/comments`,
            method: "POST",
            data: data
        })
            .done(function (msg) {
                renderData(msg.comments);
            })
            .fail(function (response) {
                let errors = response.responseJSON.errors;
                renderErrors(errors);
            });
    });

    function clearForm() {
        $('.clear-fom').trigger('reset')
    }

    function renderData(html) {
        let commentsBlock = $('.scrollspy-example');
        $(commentsBlock).append(html);
        clearForm();
    }

    function renderErrors(errors) {
        let keys = Object.keys(errors);
        for (let i = 0; i < keys.length; i++) {
            let key = keys[i];
            let element = $(`#${key}`);
            $(element).addClass('is-invalid');

            let html = `<ul class="errors-${key}">`;
            for (let j = 0; j < errors[key].length; j++) {
                html += `<li>${errors[key][j]}</li>`;
            }
            html += '</ul>';
            $(element).parent().append(html);
        }
    }

    $('.body').on('change', function (event) {
        if ($(this).hasClass('is-invalid')) {
            $('.errors-body').remove();
        }
    });

    $('.edit-comment').click(function (event) {
        const commentId = $(this).attr('data-comment-id');
        $(`#comment-e-f${commentId}`).removeClass('edit-form');
        $(this).hide();
    });

    $('.update-comment').click(function (e) {
        e.preventDefault();
        const commentId = $(this).attr('data-comment-id');
        const photoId = $(this).attr('data-photo-id');
        const block = $(`.comment-block-${commentId}`);
        const data = $(`#edit-comment-form-${commentId}`).serialize();
        console.log(block);
        $.ajax({
            url: `${photoId}/comments/${commentId}`,
            method: "PUT",
            data: data

        })
            .done(response => {
                $(block).empty();
                $(block).append(response.comment);
            })
            .fail(response => {
                console.log(response);
            })
    });

    $('.delete').on('click', function (event) {
        const commentId = $(this).attr('data-delete-id');
        const photoId = $(this).attr('data-post-delete-id');
        const token = $('input[type=hidden]').val();
        const comment = $(`#delete-comment-${commentId}`);
        $.ajax({
            url: `${photoId}/comments/${commentId}`,
            method: 'delete',
            data: {_token: token}
        })
            .done(function(response) {
                console.log(response);
                $(comment).remove();
            })
            .fail(function(response) {
                console.log(response);
            });
    })
});