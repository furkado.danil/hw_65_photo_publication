<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Photo;
use Faker\Generator as Faker;

$factory->define(Photo::class, function (Faker $faker) {
    return [
        'picture' => $faker->image($dir = 'public/storage/images', $width = 640, $height = 480),
        'user_id' => rand(1, 5),
    ];
});
